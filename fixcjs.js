/* Node/ts-node is stupid and we have to convince it that the "require" export
in package.json's exports field is, in fact, a CommonJS file and not a module
by creating an empty package.json alongside it that doesn't have a "type":
"module" field. */

import * as fs from 'fs'
fs.closeSync(fs.openSync('dist/commonjs/package.json', 'w'))
