import { pakoDecoderSetup } from './index'


const SAMPLE_ARRAY = [0x55,0xc8,0xb1,0x0d,0x80,0x30,0x0c,0x04,0xc0,0x9e,0x29,0x7e,0x02,0x06,0x60,0x0e,0x16,0x80,0xf8,0x43,0x2c,0x25,0x36,0x8a,0x2d,0x65,0x7d,0x6a,0xae,0xbc,0xb3,0x69,0xe0,0x42,0x77,0x77,0x7b,0x50,0xb5,0x13,0x07,0x16,0x61,0xa4,0x20,0x1d,0x41,0x22,0x1b,0x21,0x5a,0x2b,0x27,0xad,0x10,0x37,0x73,0x91,0xf6,0xeb,0x44,0xf1,0xf1,0x4e,0x46,0xa8,0x1b,0x06,0xb3,0xb9,0xc4,0xbe,0x7d]
const SAMPLE = new Uint8Array(SAMPLE_ARRAY)
const SAMPLE_UNCOMPRESSED_SIZE = 94

const writeController: WritableStreamDefaultController = {
  error: (e) => {throw new Error('Sink error: ' + e)}
}


test('Inflate using Pako buffer decoder', async () => {
  const result = (await pakoDecoderSetup()).decodeBuffer(SAMPLE, SAMPLE_UNCOMPRESSED_SIZE)

  expect(await result).toMatchSnapshot('Inflated data from deflate.zip')
})

test('Inflate using Pako sink', async () => {
  const [sink, result] = (await pakoDecoderSetup()).createSink(SAMPLE_UNCOMPRESSED_SIZE)
  sink.write?.(SAMPLE, writeController)
  sink.close?.()

  expect(await result).toMatchSnapshot('Inflated data from deflate.zip')
})

test('Inflate using Pako with streamed chunks', async () => {
  const raw1 = SAMPLE.slice(0, 40)
  const raw2 = SAMPLE.slice(40)
  const [sink, result] = (await pakoDecoderSetup()).createSink(SAMPLE_UNCOMPRESSED_SIZE)
  sink.write?.(new Uint8Array(raw1), writeController)
  sink.write?.(new Uint8Array(raw2), writeController)
  sink.close?.()

  expect(await result).toMatchSnapshot('Inflated and chunked data from deflate.zip')
})

test('Inflate using Pako Transformer', async () => {
  const raw1 = SAMPLE.slice(0, 40)
  const raw2 = SAMPLE.slice(40)
  const outChunks: Uint8Array[] = []
  const controller: TransformStreamDefaultController<Uint8Array> = {
    enqueue: (chunk: Uint8Array) => outChunks.push(chunk),
    // The following are unused but required
    desiredSize: 0,
    error: () => {},
    terminate: ()=> {}
  }
  const transformer = (await pakoDecoderSetup()).createTransformer(SAMPLE_UNCOMPRESSED_SIZE)
  // transform() and flush() are known to be present
  /* eslint-disable @typescript-eslint/no-non-null-assertion */
  transformer.transform!(new Uint8Array(raw1), controller)
  transformer.transform!(new Uint8Array(raw2), controller)
  transformer.flush!(controller)
  /* eslint-enable @typescript-eslint/no-non-null-assertion */

  expect(outChunks).toMatchSnapshot('Inflated transformed chunks from deflate.zip')
})

test('Inflate using Pako with invalid data throws error', async () => {
  // Cut off the first half the sample, making the remaining invalid
  const [sink] = (await pakoDecoderSetup()).createSink(0)
  expect(() => sink.write?.(SAMPLE, writeController)).toThrow()
})
