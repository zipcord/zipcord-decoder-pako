/** Implementation of a Zipcord [[`DecoderConfig`]] using Pako to decode data.

  Pako is included in the Zipcord source and has been modified to strip out
  unused code and to support Streams.

  @packageDocumentation
 */

import { decodeBuffer, createSink, createTransformer } from './pako-inflate/index.js'


/** Setup function for ZipcordOptions.decoderSetup */
export async function pakoDecoderSetup(): Promise<{
  createSink: (uncompressedSize: number) => [sink: UnderlyingSink<Uint8Array>, output: Promise<Uint8Array>]
  createTransformer: (uncompressedSize: number) => Transformer<Uint8Array, Uint8Array>
  decodeBuffer: (buffer: Uint8Array, uncompressedSize: number) => Promise<Uint8Array>
}> {
  return { createSink, createTransformer, decodeBuffer }
}
