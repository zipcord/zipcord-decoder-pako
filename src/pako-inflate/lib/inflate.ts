// (C) 1995-2013 Jean-loup Gailly and Mark Adler
// (C) 2014-2017 Vitaly Puzrin and Andrey Tupitsin
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//   claim that you wrote the original software. If you use this software
//   in a product, an acknowledgment in the product documentation would be
//   appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//   misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
// Modified 2020 by David Powell from lib/inflate.js in the original Pako
// source. Converted to TypeScript and removed code irrelevant to the decoder.

import * as zlib_inflate from './zlib/inflate.js'
import * as c from './zlib/constants.js'
import ZStream from './zlib/zstream.js'


const NULL_BUFFER = new Uint8Array()
const OUTPUT_CHUNK_SIZE = 16384
const WINDOW_BITS = -15

const MESSAGES = new Map([
  [2, 'need dictionary'],      // Z_NEED_DICT
  [1, 'stream end'],           // Z_STREAM_END
  [0, 'no error detected'],    // Z_OK
  [-1, 'file error'],          // Z_ERRNO
  [-2, 'stream error'],        // Z_STREAM_ERROR
  [-3, 'data error'],          // Z_DATA_ERROR
  [-4, 'insufficient memory'], // Z_MEM_ERROR
  [-5, 'buffer error'],        // Z_BUF_ERROR
  [-6, 'incompatible version'] // Z_VERSION_ERROR
])

/** Streaming Deflate decoder based on Pako's Inflate class */
export class PakoInflator {
  ended = false
  protected zlibStream: ZStream

  constructor() {
    this.zlibStream = new ZStream()
    this.zlibStream.avail_out = 0

    const initStatus = zlib_inflate.inflateInit2(this.zlibStream, WINDOW_BITS)
    if (initStatus !== c.Z_OK) {
      throw new Error(`Could not initialize zlib: (${initStatus}) ${MESSAGES.get(initStatus) ?? '<Unknown status code>'}`)
    }
  }

  /** Inflate chunk and call queueOutputChunk with decoded data */
  decode(chunk: Uint8Array, queueOutputChunk: (outChunk: Uint8Array) => void, mode = c.Z_NO_FLUSH): void {
    let status

    /** Flag to properly process Z_BUF_ERROR on testing inflate call
      when we check that all output data was flushed, see below for details */
    let allowBufError = false

    if (this.ended) {
      throw new Error('Cannot decode after stream is finished')
    }

    this.zlibStream.input = chunk
    this.zlibStream.next_in = 0
    this.zlibStream.avail_in = this.zlibStream.input.length

    do {
      if (this.zlibStream.avail_out === 0) {
        this.zlibStream.output = new Uint8Array(OUTPUT_CHUNK_SIZE)
        this.zlibStream.next_out = 0
        this.zlibStream.avail_out = OUTPUT_CHUNK_SIZE
      }

      status = zlib_inflate.inflate(this.zlibStream, c.Z_NO_FLUSH)    /* no bad return value */

      if (status === c.Z_BUF_ERROR && allowBufError === true) {
        status = c.Z_OK
        allowBufError = false
      }

      if (status !== c.Z_STREAM_END && status !== c.Z_OK) {
        this.ended = true
        throw new Error(this.zlibStream.msg)
      }

      if (this.zlibStream.next_out) {
        if (
          this.zlibStream.avail_out === 0 ||
          status === c.Z_STREAM_END ||
          (this.zlibStream.avail_in === 0 && mode === c.Z_FINISH)
        ) {
          // output is known to not be null at this point
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          const zlibOutput = this.zlibStream.output!
          const outBuffer = zlibOutput.length === this.zlibStream.next_out ?
            zlibOutput : zlibOutput.subarray(0, this.zlibStream.next_out)
          queueOutputChunk(outBuffer)
        }
      }

      // When no more input data, we should check that internal inflate buffers
      // are flushed. The only way to do it when avail_out = 0 - run one more
      // inflate pass. But if output data not exists, inflate return Z_BUF_ERROR.
      // Here we set flag to process this error properly.
      //
      // NOTE. Deflate does not return error in this case and does not needs such
      // logic.
      if (this.zlibStream.avail_in === 0 && this.zlibStream.avail_out === 0) {
        allowBufError = true
      }
    } while (
      (this.zlibStream.avail_in > 0 || this.zlibStream.avail_out === 0) &&
      status !== c.Z_STREAM_END
    )

    // Finalize on the last chunk.
    if (status === c.Z_STREAM_END || mode === c.Z_FINISH) {
      const finalStatus = zlib_inflate.inflateEnd(this.zlibStream)
      this.ended = true
      if (finalStatus !== c.Z_OK) {
        throw new Error(this.zlibStream.msg)
      }
      return
    }
  }

  /** Ensure all data is flushed from output buffers */
  finalize(queueOutputChunk: (outChunk: Uint8Array) => void): void {
    // Pako's documentation says to process the final chunk with the c.Z_FINISH
    // mode. However, you don't know when a Javascript stream is ending until
    // it's over, so we send one last empty chunk with the flag set. We also
    // need to check the ended flag in case the stream was finalized already;
    // Pako/zlib seems to detect the end of the stream on its own and probably
    // already did this. I don't really understand how the internals of how
    // zlib/pako work, so in the interest of not breaking things just because
    // it seemed to work on my test files check if it was actually ended and
    // push an empty buffer with the Z_FINISH flag set as pako expects.
    if (!this.ended) {
      this.decode(NULL_BUFFER, queueOutputChunk, c.Z_FINISH)
    }
  }
}
