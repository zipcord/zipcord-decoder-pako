import { PakoInflator } from './lib/inflate.js'


/** Create a Transformer object suitable for a TransformStream constructor */
export function createTransformer(): Transformer<Uint8Array, Uint8Array> {
  const decoder = new PakoInflator()

  return {
    transform(chunk, controller): void {
      decoder.decode(chunk, (outChunk) => controller.enqueue(outChunk))
    },

    flush(controller): void {
      decoder.finalize( (outChunk) => controller.enqueue(outChunk) )
    }
  }
}

/** Create an UnderlyingSink object for a ReadableStream constructor */
export function createSink(uncompressedSize: number): [sink: UnderlyingSink<Uint8Array>, output: Promise<Uint8Array>] {
  const decoder = new PakoInflator()
  const result = new Uint8Array(uncompressedSize)
  let promiseResolve: (output: Uint8Array) => void
  const promise = new Promise<Uint8Array>( (res) => {promiseResolve = res} )
  let resultWritten = 0

  const processOutput = (outChunk: Uint8Array): void => {
    if (resultWritten + outChunk.byteLength > uncompressedSize) {
      throw new Error(
        `Decoded chunk exceeds output buffer size (${resultWritten + outChunk.byteLength} > ${uncompressedSize})`
      )
    }
    result.set(outChunk, resultWritten)
    resultWritten += outChunk.length
  }

  const sink = {
    write(chunk: Uint8Array): void {
      decoder.decode(chunk, processOutput)
    },

    close(): void {
      decoder.finalize(processOutput)
      promiseResolve(result)
    },
  }

  return [sink, promise]
}

/** Decode an input buffer to an output buffer */
export async function decodeBuffer(input: Uint8Array, uncompressedSize: number): Promise<Uint8Array> {
  const [sink, output] = createSink(uncompressedSize)
  await sink.write?.(input, { error: (e) => {throw new Error('Could not decompress data:' + e)} })
  sink.close?.()
  return output
}
