Stripped down version of Pako, removing deflate and other code not used by
Zipcord, and converted to use Javascript Streams APIs. Original full version
of Pako can be found at https://github.com/nodeca/pako
