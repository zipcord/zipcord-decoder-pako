# zipcord-decoder-pako

Decoder for [Zipcord](https://gitlab.com/zipcord/zipcord) using
[Pako](https://github.com/nodeca/pako) as its backend. Includes a minimal
stripped-down version of Pako in the source to avoid bloating bundles with the
compression code included in the upstream version.

This was originally the default decoder for Zipcord until it was replaced with
[zipcord-decoder-fflate](https://gitlab.com/zipcord/zipcord-decoder-fflate).
Pako provides approximately the same performance as fflate but with a
significantly larger payload. See the [benchmark
page](https://zipcord.gitlab.io/zipcord-demo/bench.html) to compare decoder
efficiency.

## Usage

Install with `npm install zipcord-decoder-pako` or the equivalant for your
package manager. Use as follows:

```typescript
import { Zipcord } from 'zipcord'
import { pakoDecoderSetup } from 'zipcord-decoder-pako';
const options = {
    decoderSetup: pakoDecoderSetup
};
const archive = await Zipcord.open('https://example.com/archive.zip', options);
// Extract files as normal, now decompressing with Pako
```

## License

zipcord-decoder-pako is copyright © David Powell and is licenced under MIT. See
LICENSE.txt for the full text of the license.

[Pako](https://github.com/nodeca/pako) was written by Andrey Tupitsin & Vitaly
Puzrin and is ported from zlib written by Jean-loup Gailly & Mark Adler. A
stripped-down fork of Pako with the encoder removed is included in Zipcord's
source. `inflate.ts` is licensed under MIT and all files under `zlib` is under
the ZLIB license.
