module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: './tsconfig.eslint.json'
  },
  extends: [
    'eslint:recommended',
    'plugin:eslint-comments/recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:compat/recommended',
    'plugin:jest/recommended',
    'plugin:jest/style'
  ],
  env: {
    browser: true
  },
  // Don't lint code copied from Pako
  ignorePatterns: ['**/*.d.ts', 'src/pako-inflate/**/*.js'],
  settings: {
    // Expected to be polyfilled and ignored by the compat plugin
    polyfills: ['fetch', 'TextDecoder']
  },
  rules: {
    'eqeqeq': 'error',
    'key-spacing': 'error',
    // Allow console.log for debugging because I am lazy and bad at debuggers
    'no-console': 'warn',
    'no-multiple-empty-lines': 'error',
    'no-trailing-spaces': 'error',
    'no-unneeded-ternary': 'error',
    'prefer-const': 'warn',
    // I will die on this hill
    'semi': [
      'error',
      'never',
      {
        beforeStatementContinuationChars: 'always'
      }
    ],
    '@typescript-eslint/consistent-type-imports': 'error',
    '@typescript-eslint/explicit-function-return-type': [
      'warn',
      {
        allowExpressions: true
      }
    ],
    '@typescript-eslint/member-delimiter-style': [
      'warn',
      {
        multiline: {
          delimiter: 'none'
        }
      }
    ],
    '@typescript-eslint/naming-convention': [
      'warn',
      { selector: 'memberLike', format: ['camelCase'], leadingUnderscore: 'allow' },
      { selector: 'enumMember', format: ['PascalCase'] },
      { selector: 'property', format: null, leadingUnderscore: 'allow',
        filter: { regex: '[- ]|Range', match: true } },
      { selector: 'typeLike', format: ['PascalCase'] },
      { selector: 'variableLike', format: ['camelCase'] },
      { selector: 'variable', format: ['camelCase', 'UPPER_CASE'] },
      { selector: 'parameter', format: ['camelCase'], leadingUnderscore: 'allow' },
    ],
    '@typescript-eslint/no-empty-function': 'off',
  }
}
