export default {
  globals: {
    'ts-jest': {
      // ...
      diagnostics: {
        // Disable unnecessary warning about modules
        // https://github.com/kulshekhar/ts-jest/issues/748#issuecomment-423528659
        ignoreCodes: [151001]
      }
    }
  },
  moduleFileExtensions: ['js', 'ts'],
  transform: {
    "^.+\\.ts$": "ts-jest"
  },
}
